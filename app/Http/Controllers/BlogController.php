<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class BlogController extends Controller
{
    public function index(){
		
		return view('home.layouts.index');
	}

	public function store(){
		 $input = Input::all();

		foreach ($input['rows'] as $row) {
			$items = new Multiple([
				'user_id' => Auth::id(),
				'store' => $row['store'],
				'link' => $row['link'],
			]);
			$items->save();
		}

	}
}
