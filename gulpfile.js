var elixir = require('laravel-elixir');
var gulp = require('gulp');
var rename = require('gulp-rename');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task("copyfiles", function() {

  // Copy jQuery, Bootstrap, and FontAwesome
  gulp.src("vendor/bower_dl/jquery/dist/jquery.js")
      .pipe(gulp.dest("resources/assets/js/"));

  gulp.src("vendor/bower_dl/bootstrap/less/**")
      .pipe(gulp.dest("resources/assets/less/bootstrap"));

  gulp.src("vendor/bower_dl/bootstrap/dist/js/bootstrap.js")
      .pipe(gulp.dest("resources/assets/js/"));

  gulp.src("vendor/bower_dl/bootstrap/dist/fonts/**")
      .pipe(gulp.dest("public/assets/fonts"));

  gulp.src("vendor/bower_dl/font-awesome/less/**")
      .pipe(gulp.dest("resources/assets/less/font-awesome"));

  gulp.src("vendor/bower_dl/font-awesome/fonts/**")
      .pipe(gulp.dest("public/assets/fonts"));

  // Copy datatables
  var dtDir = 'vendor/bower_dl/datatables-plugins/integration/';

  gulp.src("vendor/bower_dl/datatables/media/js/jquery.dataTables.js")
      .pipe(gulp.dest('resources/assets/js/'));

  gulp.src(dtDir + 'bootstrap/3/dataTables.bootstrap.css')
      .pipe(rename('dataTables.bootstrap.less'))
      .pipe(gulp.dest('resources/assets/less/others/'));

  gulp.src(dtDir + 'bootstrap/3/dataTables.bootstrap.js')
      .pipe(gulp.dest('resources/assets/js/'));
	
	// Copy clean-blog less files
  gulp.src("vendor/bower_dl/clean-blog/less/**")
      .pipe(gulp.dest("resources/assets/less/clean-blog"));

	// copy formValidation
	var fvDir = 'vendor/bower_dl/form.validation/vendor/';
	
	gulp.src(fvDir + 'bootstrap/css/bootstrap.min.css')
		.pipe(rename('bootstrap.min.less'))
		.pipe(gulp.dest('resources/assets/less/others/'));
	
	gulp.src(fvDir + 'formvalidation/dist/css/formValidation.min.css')
		.pipe(rename('formValidation.min.less'))
		.pipe(gulp.dest('resources/assets/less/others/'));
		
	// gulp.src(fvDir + 'jquery/jquery.min.js')
		// .pipe(gulp.dest('resources/assets/js/'));
	
	// gulp.src(fvDir + 'bootstrap/js/bootstrap.min.js')
		// .pipe(gulp.dest('resources/assets/js/'));
		
	gulp.src(fvDir + 'formvalidation/dist/js/formValidation.min.js')
		.pipe(gulp.dest('resources/assets/js/'));
		
	gulp.src(fvDir + 'formvalidation/dist/js/framework/bootstrap.min.js')
		.pipe(gulp.dest('resources/assets/js/'));
	
	

});

/**
 * Default gulp is to run this elixir stuff
 */
elixir(function(mix) {

    // Combine scripts
    mix.scripts([
        'js/jquery.js',
        'js/bootstrap.js',
        'js/jquery.dataTables.js',
        'js/dataTables.bootstrap.js'
		// 'js/jquery.min.js'
		// 'js/bootstrap.min.js'
		'js/formValidation.min.js'
		'js/bootstrap.min.js'
    ],
    'public/assets/js/admin.js', 'resources//assets');

    // Combine blog scripts
    mix.scripts([
        'js/jquery.js',
        'js/bootstrap.js',
        'js/blog.js'
    ], 'public/assets/js/blog.js', 'resources//assets');

    // Compile CSS
    mix.less('admin.less', 'public/assets/css/admin.css');
    mix.less('blog.less', 'public/assets/css/blog.css');
});
