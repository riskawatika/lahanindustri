{{-- Navigation --}}
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
  <div class="container-fluid">
    {{-- Brand and toggle get grouped for better mobile display --}}
    <div class="navbar-header page-scroll">
      <button type="button" class="navbar-toggle" data-toggle="collapse"
              data-target="#navbar-main">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL('/home') }}">{{ config('home.name') }}</a>
    </div>

    {{-- Collect the nav links, forms, and other content for toggling --}}
    <div class="collapse navbar-collapse" id="navbar-main">
		<ul class="nav navbar-nav">
			<li><a href="{{ URL('/home')}}">Home</a></li>
			@if (Auth::check())
			<li @if (Request::is('admin/parameter*')) class="active" @endif>
				<a href="{{ URL('/admin/parameter/')}}" id="nav-post">Dashboard</a>
			</li>
			@endif
		</ul>
		<ul class="nav navbar-nav navbar-right">
			@if (Auth::guest())
				<li><a href="#modal-login" data-toggle="modal">Login</a></li>
			@else
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
					aria-expanded="false">{{ Auth::user()->name }}
					<span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ URL('/auth/logout')}}">Logout</a></li>
						</ul>
				</li>
			@endif
		</ul>
    </div>
  </div>
</nav>

<div class="modal fade" id="modal-login">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" action="{{ URL('/auth/login')}}"
            class="form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
     
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            ×
          </button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
                <label class="col-md-4 control-label">E-Mail Address</label>
                <div class="col-md-6">
                  <input type="email" class="form-control" name="email"
                         value="{{ old('email') }}" autofocus>
                </div>
         </div>
		 <div class="form-group">
                <label class="col-md-4 control-label">Password</label>
                <div class="col-md-6">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
		
        </div>
        <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			<button type="submit" class="btn btn-primary">Login</button>
        </div>
      </form>
    </div>
  </div>
</div>