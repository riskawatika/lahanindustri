<ul class="nav navbar-nav">
  <li><a href="{{ URL('/home')}}">Home</a></li>
  @if (Auth::check())
	<li @if (Request::is('admin/parameter*')) class="active" @endif>
      <a href="{{ URL('/admin/parameter')}}">Lahan</a>
    </li>
	<li @if (Request::is('admin/matrix*')) class="active" @endif>
      <a href="{{ URL('/admin/matrix')}}">Matrix</a>
    </li>
    <li @if (Request::is('admin/upload*')) class="active" @endif>
      <a href="{{ URL('/admin/upload')}}">Uploads</a>
    </li>
	<li @if (Request::is('admin/grafik*')) class="active" @endif>
      <a href="{{ URL('/admin/grafik')}}">Grafik</a>
    </li>
  @endif
</ul>

<ul class="nav navbar-nav navbar-right">
  @if (Auth::guest())
    <li><a href="{{ URL('/auth/login')}}">Login</a></li>
  @else
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
         aria-expanded="false">{{ Auth::user()->name }}
        <span class="caret"></span>
      </a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="{{ URL('/auth/logout')}}">Logout</a></li>
      </ul>
    </li>
  @endif
</ul>