@extends('admin.layout')

@section('content')
 
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-6">
        <h3>Lahan <small>» Listing</small></h3>
      </div>
      <div class="col-md-6 text-right">
        <a href="{{ URL('/admin/parameter/create')}}" class="btn btn-success btn-md">
          <i class="fa fa-plus-circle"></i> New Lahan
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">

        @include('admin.partials.errors')
        @include('admin.partials.success')

        <table id="posts-table" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Lokasi</th>
              <th>Morfologi</th>
              <th>Tujuan</th>
              <th>Kemiringan</th>
              <th>Jenis Batuan</th>
              <th>Jenis Tanah</th>
              <th>Karakteristik Air Tanah</th>
              <th>Curah Hujan</th>
              <th data-sortable="false">Actions</th>
            </tr>
          </thead>
          <tbody>
           
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script type="text/javascript">
    $(function() {
      $("#posts-table").DataTable({
        order: [[0, "desc"]]
      });
    });
  </script>
@stop
