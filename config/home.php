<?php
return [
  'name' => "Fahpzzyland",
  'title' => "Skripsi-App",
  'subtitle' => 'A clean web-app written in Laravel 5.1',
  'description' => 'This is my meta description',
  'author' => 'Tika Riskawati',
  'page_image' => 'home-bg.jpg',
  'posts_per_page' => 5,
  'uploads' => [
    'storage' => 'local',
    'webpath' => '/uploads/',
  ],
];
